Vue.component('button-simon', {
    props: ['color'],
    // data: function () {
    //     return {
    //         isHighlighted : false
    //     }
    // },
    template: `<div v-bind:id="color.id" v-on:click="$emit('click')" v-bind:style="{ backgroundColor : color.isSelected ? color.isSelectedColor : '' }"></div>`,
    // template: `<div v-bind:id="color.id" v-on:click="$emit('click')" v-bind:style="{ backgroundColor : isHighlighted ? color.isSelectedColor : '' }"></div>`,
    // methods: {
    //     //highlight div when choosed
    //     highlightDiv: function(color, i){
    //         let indiceBoucle = i+1;
    //         setTimeout(() => {
    //             this.playMusicByColor(color);
    //             this.isHighlighted = true;
    //         }, 400*indiceBoucle);

    //         setTimeout(() => {
    //             this.isHighlighted = false;
    //         }, 400*indiceBoucle+300);
    //     },

    //     //play music by color
    //     playMusicByColor: function(currentColor) {
    //         let pathFileMusic = currentColor.sound;
    //         let audio = new Audio(pathFileMusic);
    //         audio.play();
    //     },
    // }
});   

   
   