const simonBtn = Vue.component('button-simon', {
    props: ['color'],
    template: `<div v-bind:id="color.id" v-on:click="$emit('click')" v-bind:style="{ backgroundColor : color.isSelected ? color.isSelectedColor : '' }"></div>`,
    updated: function(){
        this.selectedColor(this.color);
    },
    methods: {
        //highlight div when choosed
        selectedColor: function(color){
            if(color.isSelected){
                this.playMusicByColor(color);
            }
        },

        //play music by color
        playMusicByColor: function(currentColor) {
            let pathFileMusic = currentColor.sound;
            let audio = new Audio(pathFileMusic);
            audio.play();
        },
    }
});   

export default simonBtn;
   
   