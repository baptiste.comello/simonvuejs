import simonBtn from './components/simonBtnComponent.js'
import startBtn from './components/startBtnComponent.js'

const app = new Vue({
    components : { 
        simonBtn,
        startBtn
     },
    el: '#app',
    data: {
        levelNumber: 0,
        gameIsRunning: false,
        playerPattern:[],
        colorPattern:[],
        colors: [
            {
                id: "one",
                color: "green",
                isSelected: false,
                isSelectedColor: "#86CB76",
                sound: "./sounds/green.ogg"
            },
            {
                id: "two",
                color: "red",
                isSelected: false,
                isSelectedColor: "#DA5652",
                sound: "./sounds/red.ogg"
            },
            {
                id: "three",
                color: "yellow",
                isSelected: false,
                isSelectedColor: "#DADA60",
                sound: "./sounds/yellow.ogg"
            },
            {
                id: "four",
                color: "blue",
                isSelected: false,
                isSelectedColor: "#6666DE",
                sound: "./sounds/blue.ogg"
            }
        ],
    },
    methods: {
        //choose color randomly(0,max);
        getRandomInt: function(max)
        {
            return Math.floor(Math.random() * Math.floor(max));
        },
        
        //start a new game
        startGame: function () {
            this.gameIsRunning = true;
            this.levelNumber = this.levelNumber + 1;
            this.chooseAColorRandomly();
        },

        // the color combo is correct
        isCorrectChoice: function() {
            for(var i=0; i<this.colorPattern.length; i++){
                if(this.colorPattern[i].id !== this.playerPattern[i].id){
                    this.colorPattern = [];
                    this.resetPlayerPattern();
                    return false;
                }
            }
            
            this.resetPlayerPattern();
            return true;
        },

        //player choose one color
        playerChoosedColor: function(colorChoosed){
            let currentColor = this.colors.indexOf(colorChoosed);
            this.playerPattern.push(this.colors[currentColor]);

            if(this.colorPattern.length === this.playerPattern.length){
                if(this.isCorrectChoice()){
                    this.levelNumber = this.levelNumber + 1;
                    this.chooseAColorRandomly();
                }
                else{
                    this.stopGame();
                }
            }
        },

        //reset array of player choice
        resetPlayerPattern: function(){
            this.playerPattern = [];
        },
        
        //stop a game when the player loose
        stopGame: function(){
            alert("C'est loose gg!");
            this.levelNumber = 0;
            this.gameIsRunning = false; 
        },

        //highlight div when choosed
        selectColor: function(color, i){
            let indiceBoucle = i+1;
            setTimeout(() => {
                color.isSelected = true;
            }, 400*indiceBoucle);

            setTimeout(() => {
                color.isSelected = false;
            }, 400*indiceBoucle+300);
        },

        //choose a color randomly
        chooseAColorRandomly: function(){
            let numberColor = this.getRandomInt(4);
            let currentColor = this.colors[numberColor];
            this.colorPattern.push(currentColor);

            for(var i=0; i<this.colorPattern.length; i++){
                this.selectColor(this.colorPattern[i], i);
            }
        },
    }
});