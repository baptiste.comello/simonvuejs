import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Simon from '../views/Simon.vue'
import ListOfGame from '../views/ListOfGame.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/simon',
    name: 'Simon',
    component: Simon
  },
  {
    path: '/listOfGame',
    name:'ListOfGame',
    component: ListOfGame
  }
]

const router = new VueRouter({
  routes
})

export default router
